package ru.t1.shevyreva.tm.api;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
