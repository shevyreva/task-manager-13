package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project changeProjectStatusByIndex(Integer Index, Status status);

    Project changeProjectStatusById(String id, Status status);

    boolean existsById(String id);

}
